from django.contrib import admin

# Register your models here.

from .models import Search, AnimeList


class AnimeListInline(admin.TabularInline):
    model = AnimeList
    extra = 0


class SearchInline(admin.TabularInline):
    model = Search
    extra = 0


admin.site.register(Search)
admin.site.register(AnimeList)
