from django.db import models
import uuid

# Create your models here.


class AnimeList(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=20000)
    description = models.TextField(default="Not available", blank=True, null=True)
    image = models.ImageField(upload_to="images/")
    release_date = models.CharField(max_length=20000)
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "AnimeList"


class Search(models.Model):
    id = models.AutoField(primary_key=True)
    search = models.CharField(max_length=500, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=False)
    anime_list = models.ManyToManyField(
        AnimeList, related_name="anime_list", blank=False, null=False
    )

    def __str__(self):
        return "{}".format(self.search)

    class Meta:
        verbose_name_plural = "Searches"
