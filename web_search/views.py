from django.shortcuts import render
import requests
import json
from .models import Search, AnimeList
from pprint import pprint
import uuid

# Create your views here.


def home(request):
    return render(request, "home.html", {})


def get_anime_from_JIKAN(request):
    search = request.GET.get("q")
    if search:
        url = "https://api.jikan.moe/v4/anime?q=" + search
        response = requests.get(url)
        data = response.json()

        anime_data = data["data"]

        unique_id = uuid.uuid1()

        for anime in anime_data:
            title = anime["title"]
            if anime["synopsis"] == None:
                description = "Not available"
            else:
                description = anime["synopsis"]

            if anime["images"]["webp"]["small_image_url"] == None:
                image = "https://demofree.sirv.com/nope-not-here.jpg"

            image = anime["images"]["webp"]["small_image_url"]
            if anime["year"] == None:
                if anime["aired"]["from"] == None:
                    release_date = "Not available"
                else:
                    release_date = anime["aired"]["from"][0:10]
            else:
                release_date = str(anime["year"])

            new_anime = AnimeList(
                title=title,
                description=description,
                image=image,
                release_date=release_date,
                unique_id=unique_id,
            )
            new_anime.save()
        all_anime = AnimeList.objects.all().filter(unique_id=unique_id)
        new_search = Search(search=search, unique_id=unique_id)
        new_search.save()
        new_search = Search.objects.all().get(unique_id=unique_id)
        new_search.anime_list.add(*all_anime)
        new_search.save()

        # feed all_anime to the template
        return render(request, "home.html", {"data": all_anime})

    else:
        print("no search term")
        return render(request, "home.html", {})


def sample_data_insertion(request):
    
    AnimeList.objects.create(
        title="One Piece",
        description="Monkey D. Luffy and his swashbuckling crew in their search for the ultimate treasure, the One Piece.",
        image="https://cdn.myanimelist.net/images/anime/2/73245.jpg",
        release_date="1999-10-20",
        unique_id=uuid.uuid1(),
    )
    return render(request, "sample.html", {})