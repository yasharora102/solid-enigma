from django.apps import AppConfig


class WebSearchConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "web_search"
